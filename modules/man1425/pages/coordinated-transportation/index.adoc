= Chapter 1 Coordinated Transportation

== Department of Human Services Mission

Strengthen Georgia by providing individuals and families access to services that promote self-sufficiency, independence, and protect Georgia's vulnerable children and adults.

=== Vision

Stronger families for a stronger Georgia.

=== Core Values

* Provide access to resources that offer support and empower Georgians and their families.
* Deliver services professionally and treat all clients with dignity and respect.
* Manage business operations effectively and efficiently by aligning resources across the agency.
* Promote accountability, transparency and quality in all services we deliver and programs we administer.
* Develop our employees at all levels of the agency.

It is the policy of the Georgia Department of Human Services (DHS) to provide quality and cost-effective transportation to eligible consumers to access Holman services and resources designed to enhance health, independence and self-sufficiency.
Coordinated Transportation allows for greater access to human services for the elderly, the disabled, and those with limited transportation options, while encouraging a more efficient method of mobility.
It eliminates duplicated transportation efforts and more effectively utilizes the available resources.
Transportation services are designed, coordinated, and monitored through the Regional Transportation Office (RTO) staff assigned to each DHS region.

Coordinated Transportation serves DHS, which includes the Division of Aging Services (DAS), Division of Family and Children Services (DFCS), and Division of Child Support Services (DCSS).
The Georgia Vocational Rehabilitation Agency (GVRA) and the Department of Behavioral Health and Developmental Disabilities (DBHDD) are also served by the DHS Coordinated Transportation System.

Ongoing success of Coordinated Transportation depends on the education and support of all partners.
Support of Coordinated Transportation is paramount to achieving the overall goal of operating an efficient transportation system and departments.
It is also essential that consistent information is shared, and participation exists throughout the network.

*Throughout this manual, the terms Transportation Provider (TP), contractor, and Prime Contractor are used interchangeably.
Depending on the service area, a Prime Contractor may be the transportation provider, or the Prime Contractor may use Subcontractors to provide services.*

== DHS Transportation Network Structure

Coordinated Transportation is administered through the DHS Office of Facilities and Support Services (OFSS), Transportation Services Section (TSS).
Actual services are provided through contracted providers in each region.
Contractors may be a state entity, county, regional government entity, private non-profit, or a for-profit vendor.
TSS manages the transportation provider contracts and provides region and department level leadership in this effort.

TSS operates in each of the 12 DHS regions.
District Operations Managers (DOM) oversee districts and provide oversight for each RTO and the counties within their service areas.

A Regional Coordinator (RC) and a Planner are assigned to one or more DHS regions.
Through its regional staff, the TSS extends its program management and oversight to the local level and incorporates local input into the system design.
The RTO, in concert with a Regional Transportation Coordinating Committee (RTCC), is responsible for transportation planning.

== Regional Transportation Coordinating Committee (RTCC)

The purpose of the RTCC is to provide local information, advice, direction, and support to the RTO.
Members of the RTCC are responsible for keeping their respective networks informed.
The RTO is responsible for educating the RTCC on Coordinated Transportation matters.
At a minimum, a RTCC includes the following (or their designated representative) within each DHS region.

[%hardbreaks]
*A* - Indicates a person that is included in an advisory capacity for the group
*V* – Voting member; each member category is limited to a designated number of votes
*V** - Indicates an Invitee that cannot vote during the contract evaluation and renewal decision

[%header,cols="3,^1,^1,^1",stripes=odd]
|===
| Representatives
| A +
Advisory
| V +
Voting | V* +
Voting +
(excluded from evaluation & renewal decisions)

s| Division of Aging (DAS) (One voting member):
|
|
|

| Aging Services Coordinator (A)
| ✓
|
|

| Regional Coordinator (V)
|
| ✓
|

| Program Manager (V)
|
| ✓
|

| Area Agency on Aging (AAA) Director (V)
|
| ✓
|

s| Division of Family and Children Services (DFCS) (Two voting members):
|
|
|

| Provider Operations Coordinator (A)
| ✓
|
|

| OFI Regional/District Manager (V)
|
| ✓
|

| Resource Coordinator (V)
|
| ✓
|

| TANF Supervisor (V)
|
| ✓
|

| SNAP Supervisor (where applicable) (V)
|
| ✓
|

s| Department of Behavioral Health and Developmental Disabilities (DBHDD) (Two voting members):
|
|
|

| Risk Management & Facilities Director (A)
| ✓
|
|

| Behavioral Health Regional Manager(s) (V)
|
| ✓
|

| Developmental Disabilities Regional Manager(s) (V)
|
| ✓
|

| ADA Contact at Regional or CSB Level (V)
|
| ✓
|

| Community Service Board Executive Director(s) (V)
|
| ✓
|

s| Georgia Vocational Rehabilitation Agency (GVRA) (One voting member):
|
|
|

| Regional Manager (V)
|
| ✓
|

| Regional Contract Specialist (V)
|
| ✓
|

| Hub or Unit Managers (V)
|
| ✓
|

s| Regional Commissions (One voting member):
|
|
|

| Executive Director (V*)
|
|
| ✓

| Transit Planners (V*)
|
|
| ✓

| Mobility Managers (V*)
|
|
| ✓

| Contract Coordinators (V*)
|
|
| ✓

s| Georgia Department of Transportation (GDOT) (One voting member):
|
|
|

| Division of Intermodal, Rural Transit Group Leader (A)
| ✓
|
|

| District Transit Managers (V)
|
| ✓
|

| Regional Transit Managers (A)
| ✓
|
|

s| Other:
|
|
|

| TSS Regional Office Staff (V)
|
| ✓
|

| TSS District Operations Manager (A)
| ✓
|
|

| TSS Atlanta staff (A)
| ✓
|
|

| Metropolitan Planning Organization (MPO) representatives (A)
| ✓
|
|

| Transportation Providers/Contractors (V*)
|
|
| ✓
|===

If a member of the RTCC chooses to appoint other staff to represent his/her organization on the RTCC, the appointed representative is responsible for keeping the RTCC member informed.

*Any Member Representing an Organization That is Directly Involved with a Contracted Interest is Subject to Disqualification from Serving on The RTCC as a Voting Member.*

The RTO staff convenes, facilitates, and records meetings of the RTCC.
RTCC meetings are held a minimum of three times per year in each region and conducted using a common agenda.
The agenda may be expanded to include district and regionally pertinent items.
Meeting minutes are distributed to the RTCC members after each meeting.
A record of attendance and non-attendance will be maintained by the RTO.
The RTO will submit a Non-Attendance list to the TSS Section Manager and the State Operations Managers after the final RTCC meeting.

RTCCs may establish advisory level committees for the purpose of providing advice and opinions on transportation issues.
The committees may meet routinely to discuss implementation and operational concerns and report concerns to the RTCC.
The TPs, Prime Contractor(s) and/or Subcontractor(s), at the discretion of the advisory committee, may be expected to attend advisory committee meetings to provide input and updates.
