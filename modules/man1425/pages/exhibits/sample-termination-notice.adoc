= Exhibit 7 Sample Termination Notice

== Sample Termination Notice

“Pursuant to clause No.
, Termination, this contract is hereby terminated immediately.
You are directed to immediately stop all work, terminate subcontracts, cease in delivering coordinated transportation services, and accept no further trip orders.
In accordance with this Notice of Termination, you must:

. Keep adequate records of your compliance with this notice, including the extent of completion on the date of this Termination;
. Immediately notify all subcontractors and suppliers, if any, of this Notice of Termination;
. Notify the Georgia Department of Human Services' contract administrator, [insert name] of any and all matters that may be adversely affected by this termination;
and
. Take any other action required by the Georgia Department of Human Services' contract administrator to expedite this termination.”
