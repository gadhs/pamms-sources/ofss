= Exhibit 4 Sample Quarterly Report Card

image::1425-539.png[,474,718]

This represents one page of the evaluation document.
Areas of performance evaluation can include: Customer service, Timely and Accurate Invoicing and Reporting, Vehicle Management, Complaint Resolution, Accident and Incident Reporting, Contract Communications, and Staffing.
